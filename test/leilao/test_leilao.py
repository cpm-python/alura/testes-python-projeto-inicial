from unittest import TestCase

from src.leilao.erros import LanceInvalido
from src.leilao.lance import Lance
from src.leilao.leilao import Leilao
from src.leilao.usuario import Usuario


class TestLeilao(TestCase):

    def setUp(self):
        self.menor_valor_esperado = 100
        self.maior_valor_esperado = 150

        self.usuario_gui = Usuario("Gui", 200)

        self.lance_yuri = Lance(Usuario("Yuri", 200), self.menor_valor_esperado)
        self.lance_gui = Lance(self.usuario_gui, self.maior_valor_esperado)

        self.leilao = Leilao("Celular")

    def test_deve_retornar_o_menor_e_o_maior_valor_de_um_lance_quando_adicionados_em_ordem_crescente(self):
        self.leilao.prope(self.lance_yuri)
        self.leilao.prope(self.lance_gui)

        self.assertEqual(self.menor_valor_esperado, self.leilao.menor_lance)
        self.assertEqual(self.maior_valor_esperado, self.leilao.maior_lance)


    def test_nao_deve_permitir_ordem_decrescente(self):
        with self.assertRaises(LanceInvalido):
            self.leilao.prope(self.lance_gui)
            self.leilao.prope(self.lance_yuri)

            self.assertEqual(self.menor_valor_esperado, self.leilao.menor_lance)
            self.assertEqual(self.maior_valor_esperado, self.leilao.maior_lance)

    def test_deve_retornar_o_mesmo_valor_quando_tiver_apenas_um_lance(self):
        self.leilao.prope(self.lance_yuri)

        self.assertEqual(self.menor_valor_esperado, self.leilao.menor_lance)
        self.assertEqual(self.menor_valor_esperado, self.leilao.maior_lance)

    def test_deve_retornar_o_menor_e_o_maior_quando_tiver_3_lances(self):

        lance_vini = Lance(Usuario("Vini", 200), 125)

        self.leilao.prope(self.lance_yuri)
        self.leilao.prope(lance_vini)
        self.leilao.prope(self.lance_gui)

        self.assertEqual(self.menor_valor_esperado, self.leilao.menor_lance)
        self.assertEqual(self.maior_valor_esperado, self.leilao.maior_lance)

    def test_deve_permitir_propor_lance_caso_o_leilao_nao_tenha_lances(self):
        self.leilao.prope(self.lance_gui)

        quantidade_de_lances_recebidos = len(self.leilao.lances)

        self.assertEqual(1, quantidade_de_lances_recebidos)

    def test_deve_permitir_propor_caso_o_ultimo_usuario_seja_diferente(self):
        self.leilao.prope(self.lance_yuri)
        self.leilao.prope(self.lance_gui)

        self.assertEqual(2, len(self.leilao.lances))

    def test_nao_poode_repetir_lance_do_ultimo_usuario(self):
        try:
            self.leilao.prope(self.lance_gui)
            self.leilao.prope(Lance(self.usuario_gui, 200.0))

            self.fail(msg="Não lançou exceção")
        except LanceInvalido:
            self.assertEqual(1, len(self.leilao.lances))

    def test_nao_poode_repetir_lance_do_ultimo_usuario_excecao(self):
        with self.assertRaises(LanceInvalido):
            self.leilao.prope(self.lance_gui)
            self.leilao.prope(Lance(self.usuario_gui, 200.0))
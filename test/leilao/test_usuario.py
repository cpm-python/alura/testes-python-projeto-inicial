from src.leilao.erros import LanceInvalido
from src.leilao.leilao import Leilao
from src.leilao.usuario import Usuario
import pytest

@pytest.fixture
def vini():
    return Usuario('Vini', 100)

@pytest.fixture
def leilao():
    return Leilao("iPhone")

def test_deve_subtrair_valor_da_carteira_quando_propor_lance(vini, leilao):

    vini.propoe_lance(leilao, 50.0)

    assert vini.carteira == 50.0

def test_deve_aceitar_lance_quando_valor_eh_menor_que_carteira(vini, leilao):

    vini.propoe_lance(leilao, 1.0)

    assert vini.carteira == 99,0

def test_deve_permitir_lance_com_valor_da_carteira(vini, leilao):
    vini.propoe_lance(leilao, 100.0)

    assert vini.carteira == 0,0

def test_nao_deve_aceita_lance_maior_que_a_carteira(vini, leilao):
    with pytest.raises(LanceInvalido):
        vini.propoe_lance(leilao, 100.1)

def test_deve_subtrair_valor_da_carteira_do_usuario_quando_este_propor_um_lance(vini, leilao):
    vini.propoe_lance(leilao, 50.0)

    assert vini.carteira == 50.0

def test_deve_permitir_propor_lance_quando_o_valor_eh_menor_que_o_valor_da_carteira(vini, leilao):
    vini.propoe_lance(leilao, 1.0)

    assert vini.carteira == 99.0

def test_deve_permitir_propor_lance_quando_o_valor_eh_igual_ao_valor_da_carteira(vini, leilao):
    vini.propoe_lance(leilao, 100.0)

    assert vini.carteira == 0.0
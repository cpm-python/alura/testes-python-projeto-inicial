from src.leilao.erros import LanceInvalido
from src.leilao.lance import Lance


class Leilao:

    def __init__(self, descricao):
        self.descricao = descricao
        self.__lances = []
        self.maior_lance = 0.0
        self.menor_lance = 0.0

    def prope(self, lance: Lance):
        if self._lance_eh_valido(lance):
            if not self._tem_lances():
                self.menor_lance = lance.valor

            self.maior_lance = lance.valor

            self.__lances.append(lance)

    @property
    def lances(self):
        return self.__lances[:]

    def _tem_lances(self):
        return self.__lances

    def _usuarios_diferente(self, lance):
        if self.lances[-1].usuario != lance.usuario:
            return True
        else:
            raise LanceInvalido("O mesmo usuário nao pode dar 2 lances seguidos não senhor")

    def _valor_maior_que_o_lance_anterior(self, lance):
        if lance.valor > self.__lances[-1].valor:
            return True
        else:
            raise LanceInvalido("O valor do lance deve ser maior que o anterior")

    def _lance_eh_valido(self, lance: Lance):
        return not self._tem_lances() or (self._usuarios_diferente(lance)
                                          and self._valor_maior_que_o_lance_anterior(lance))

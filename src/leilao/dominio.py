from src.leilao.avaliador import Avaliador
from src.leilao.lance import Lance
from src.leilao.leilao import Leilao
from src.leilao.usuario import Usuario

gui = Usuario("Gui")
yuti = Usuario("Yuri")

lance_yuri = Lance(yuti, 100)
lance_gui = Lance(gui, 150)

leilao = Leilao("Celular")
leilao.prope(lance_yuri)
leilao.prope(lance_gui)

for lance in leilao.lances:
    print(f'O usuário {lance.usuario.nome} deu o lance de R$ {lance.valor}')

print(leilao)

print(f'O menor lance foi de {leilao.menor_lance} e o maior lance foi {leilao.maior_lance}')
from src.leilao.erros import LanceInvalido
from src.leilao.lance import Lance
from src.leilao.leilao import Leilao


class Usuario:

    def __init__(self, nome, carteira):
        self.__nome = nome
        self.__carteira = carteira

    @property
    def nome(self):
        return self.__nome

    @property
    def carteira(self):
        return self.__carteira

    def propoe_lance(self, leilao: Leilao, valor):
        if not self._o_valor_e_valido(valor):
            raise LanceInvalido('Deu ruim')

        lance = Lance(self, valor)
        leilao.prope(lance)

        self.__carteira -= valor

    def _o_valor_e_valido(self, valor):
        return valor <= self.__carteira